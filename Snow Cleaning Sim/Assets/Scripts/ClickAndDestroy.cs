using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickAndDestroy : MonoBehaviour
{
    public GameObject Snow;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Destroy(Snow);
        }
    }
}
